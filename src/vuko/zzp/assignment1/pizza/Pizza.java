package vuko.zzp.assignment1.pizza;

import vuko.zzp.assignment1.builder.PizzaBuilder;

import java.util.LinkedList;
import java.util.List;

public class Pizza {
    private final String size;
    private final String necessaryIngredient;
    private final String sauce;
    private final List<String> optionalIngredients;


    protected Pizza(PizzaBuilder pizzaBuilder) {
        this.size = pizzaBuilder.getSize();
        this.necessaryIngredient = pizzaBuilder.getNecessaryIngredient();
        this.sauce = pizzaBuilder.getSauce();
        this.optionalIngredients = new LinkedList<>(pizzaBuilder.getOptionalIngredients());
    }
}
