package vuko.zzp.assignment1.pizza;

import vuko.zzp.assignment1.builder.PizzaBuilder;

public class CalzonePizza extends Pizza {
    private final Boolean isSauceOutside;

    public CalzonePizza(PizzaBuilder pizzaBuilder) {
        super(pizzaBuilder);
        isSauceOutside = pizzaBuilder.isTypeCalzone();
    }
}
