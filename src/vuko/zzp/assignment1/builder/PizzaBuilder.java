package vuko.zzp.assignment1.builder;

import vuko.zzp.assignment1.pizza.Pizza;

import java.util.LinkedList;
import java.util.List;

public abstract class PizzaBuilder {


    private final String size;
    private final String necessaryIngredient;
    private final String sauce;
    private final boolean isTypeCalzone;


    private List<String> optionalIngredients;

    public PizzaBuilder(String size, String necessaryIngredient, String sauce, boolean isTypeCalzone) {
        this.size = size;
        this.necessaryIngredient = necessaryIngredient;
        this.sauce = sauce;
        this.isTypeCalzone = isTypeCalzone;
        this.optionalIngredients = new LinkedList<>();
    }


    public boolean isTypeCalzone() {
        return isTypeCalzone;
    }

    public String getSize() {
        return size;
    }

    public String getNecessaryIngredient() {
        return necessaryIngredient;
    }

    public String getSauce() {
        return sauce;
    }

    public List<String> getOptionalIngredients() {
        return optionalIngredients;
    }

    public PizzaBuilder addOptionalIngredient(String optionalIngredient) {
        this.optionalIngredients.add(optionalIngredient);
        return this;
    }

    abstract Pizza buildPizza();
}
