package vuko.zzp.assignment1.builder;

import vuko.zzp.assignment1.pizza.CalzonePizza;
import vuko.zzp.assignment1.pizza.Pizza;

public class CalzonePizzaBuilder extends PizzaBuilder {
    private boolean isSauceOutside;

    public CalzonePizzaBuilder(String size, String necessaryIngredient, String sauce, boolean isSauceOutside) {
        super(size, necessaryIngredient, sauce, true);
        this.isSauceOutside = isSauceOutside;
    }

    public boolean isSauceOutside() {
        return isSauceOutside;
    }

    @Override
    public Pizza buildPizza() {
        return new CalzonePizza(this);
    }
}
