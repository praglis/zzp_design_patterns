package vuko.zzp.assignment1.builder;

import vuko.zzp.assignment1.pizza.NewyorkerPizza;
import vuko.zzp.assignment1.pizza.Pizza;

public class NewyorkerPizzaBuilder extends PizzaBuilder {
    public NewyorkerPizzaBuilder(String size, String necessaryIngredient, String sauce) {
        super(size, necessaryIngredient, sauce, false);
    }

    @Override
    public Pizza buildPizza() {
        return new NewyorkerPizza(this);
    }
}
