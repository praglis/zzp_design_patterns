package vuko.zzp.assignment3.restaurant.drink;

import java.math.BigDecimal;

public class Sprite extends CarbonatedBeverage {
    public Sprite() {
        setCost(new BigDecimal("2.17"));
    }
}
