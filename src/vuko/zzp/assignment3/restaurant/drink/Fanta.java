package vuko.zzp.assignment3.restaurant.drink;

import java.math.BigDecimal;

public class Fanta extends CarbonatedBeverage {
    public Fanta(){
        setCost(new BigDecimal("4.49"));
    }
}
