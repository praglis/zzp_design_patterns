package vuko.zzp.assignment3.restaurant.drink;

import java.math.BigDecimal;

public class Pepsi extends CarbonatedBeverage {
    public Pepsi(){
        setCost(new BigDecimal("2.88"));
    }
}
