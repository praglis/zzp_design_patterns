package vuko.zzp.assignment3.restaurant;

import vuko.zzp.assignment3.restaurant.command.*;
import vuko.zzp.assignment3.restaurant.discount.AboveSumDiscount;
import vuko.zzp.assignment3.restaurant.discount.DiscountStrategy;
import vuko.zzp.assignment3.restaurant.discount.SameProductDiscount;
import vuko.zzp.assignment3.restaurant.observer.Observer;
import vuko.zzp.assignment3.restaurant.observer.Subject;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public class OrderMachine implements Subject {
    Order order;
    private List<Observer> observers = new LinkedList<>();

    public void newOrder() {
        this.order = new Order();
    }

    public void addPepsi() {
        new OrderPepsi(order).execute();
    }

    public void addSprite() {
        new OrderSprite(order).execute();
    }

    public void addFanta() {
        new OrderFanta(order).execute();
    }

    public void addHamburger() {
        new OrderHamburger(order).execute();
    }

    public void addCheeseburger() {
        new OrderCheeseburger(order).execute();
    }

    public void addFishburger() {
        new OrderFishburger(order).execute();
    }

    public void finalizeOrder() {
        System.out.println("Koszt zamówienia " + order.hashCode() + " bez zniżek: " + order.getOrderCost());

        DiscountStrategy aboveSumStrategy = new AboveSumDiscount();
        DiscountStrategy sameProductStrategy = new SameProductDiscount();

        BigDecimal aboveSumDiscount = aboveSumStrategy.calculateDiscount(order);
        BigDecimal sameProductDiscount = sameProductStrategy.calculateDiscount(order);


        order.setOrderCost(order.getOrderCost().subtract(aboveSumDiscount).subtract(sameProductDiscount));
        System.out.println("Koszt zamówienia:" + order.hashCode() + " ze zniżkami: " + order.getOrderCost());
        notifyObservers();
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void notifyObservers() {
        observers.forEach(observer -> observer.update(order));
    }
}
