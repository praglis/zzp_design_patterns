package vuko.zzp.assignment3.restaurant.burger;

import java.math.BigDecimal;

public class Hamburger extends Burger {
    public Hamburger(){
        setCost(new BigDecimal("19.00"));
    }
}
