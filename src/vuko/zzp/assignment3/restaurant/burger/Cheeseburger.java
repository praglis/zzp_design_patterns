package vuko.zzp.assignment3.restaurant.burger;

import java.math.BigDecimal;

public class Cheeseburger extends Burger {
    public Cheeseburger(){
        setCost(new BigDecimal("3.50"));
    }
}
