package vuko.zzp.assignment3.restaurant.burger;

import java.math.BigDecimal;

public class Fishburger extends Burger {
    public Fishburger(){
        setCost(new BigDecimal("8.10"));
    }
}
