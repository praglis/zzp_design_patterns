package vuko.zzp.assignment3.restaurant;

import java.math.BigDecimal;

public abstract class Product {
    BigDecimal cost;

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }
}
