package vuko.zzp.assignment3.restaurant.discount;

import vuko.zzp.assignment3.restaurant.Order;

import java.math.BigDecimal;

public interface DiscountStrategy {
    BigDecimal calculateDiscount(Order order);
}
