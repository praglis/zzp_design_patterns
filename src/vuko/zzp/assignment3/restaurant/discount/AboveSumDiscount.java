package vuko.zzp.assignment3.restaurant.discount;

import vuko.zzp.assignment3.restaurant.Order;
import vuko.zzp.assignment3.restaurant.command.Command;
import vuko.zzp.assignment3.restaurant.drink.CarbonatedBeverage;

import java.math.BigDecimal;

public class AboveSumDiscount implements DiscountStrategy {
    @Override
    public BigDecimal calculateDiscount(Order order) {
        BigDecimal beverageCost = BigDecimal.ZERO;
        boolean hasCarbonatedBeverage = false;

        for (Command orderCommand : order.getOrderHistory()) {
//            orderCost.add(orderCommand.getProduct().getCost());
            if (orderCommand.getProduct() instanceof CarbonatedBeverage) {
                hasCarbonatedBeverage = true;
                beverageCost = orderCommand.getProduct().getCost();
            }
        }
        if (!hasCarbonatedBeverage) return BigDecimal.ZERO;
        if (order.getOrderCost().compareTo(BigDecimal.valueOf(30)) >= 0)
            return beverageCost.subtract(new BigDecimal("0.01"));
        else return BigDecimal.ZERO;
    }
}
