package vuko.zzp.assignment3.restaurant.discount;

import vuko.zzp.assignment3.restaurant.Order;
import vuko.zzp.assignment3.restaurant.burger.Burger;
import vuko.zzp.assignment3.restaurant.command.Command;
import vuko.zzp.assignment3.restaurant.drink.CarbonatedBeverage;

import java.math.BigDecimal;

public class SameProductDiscount implements DiscountStrategy {
    @Override
    public BigDecimal calculateDiscount(Order order) {
        BigDecimal cheapestProduct = order.getOrderHistory().get(0).getProduct().getCost();
        int burgerCount = 0, beverageCount = 0;

        for (Command command : order.getOrderHistory()) {
            if (command.getProduct().getCost().compareTo(cheapestProduct) < 0) {
                cheapestProduct = command.getProduct().getCost();

            }
            if (command.getProduct() instanceof Burger) burgerCount++;
            else if (command.getProduct() instanceof CarbonatedBeverage) beverageCount++;
        }
        if (beverageCount > 2 || burgerCount > 2) {
            return cheapestProduct.multiply(BigDecimal.valueOf(0.1));
        } else return BigDecimal.ZERO;
    }
}
