package vuko.zzp.assignment3.restaurant.observer;

import vuko.zzp.assignment3.restaurant.Order;

public interface Observer {
    void update(Order order);
}
