package vuko.zzp.assignment3.restaurant.observer;

public interface Subject {
    void registerObserver(Observer o);

    void notifyObservers();
}