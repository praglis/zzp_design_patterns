package vuko.zzp.assignment3.restaurant.observer;

import vuko.zzp.assignment3.restaurant.Order;

public class KitchenDepartment implements Observer {

    @Override
    public void update(Order order) {
        System.out.println("Dział kuchni został poinformowany o zamówieniu: " + order.hashCode());
    }
}
