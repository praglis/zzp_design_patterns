package vuko.zzp.assignment3.restaurant.command;

import vuko.zzp.assignment3.restaurant.Order;
import vuko.zzp.assignment3.restaurant.Product;
import vuko.zzp.assignment3.restaurant.burger.Hamburger;

public class OrderHamburger implements Command{
    Order order;
    Product product;

    public OrderHamburger(Order order){
        this.order = order;
        product = new Hamburger();
    }

    @Override
    public void execute() {
        order.addProduct(product);
        order.addCommandToHistory(this);
    }

    @Override
    public Product getProduct() {
        return product;
    }
}
