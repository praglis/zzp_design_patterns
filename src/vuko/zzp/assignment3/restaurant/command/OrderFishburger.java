package vuko.zzp.assignment3.restaurant.command;

import vuko.zzp.assignment3.restaurant.Order;
import vuko.zzp.assignment3.restaurant.Product;
import vuko.zzp.assignment3.restaurant.burger.Fishburger;

public class OrderFishburger implements Command {
    Order order;
    Product product;

    public OrderFishburger(Order order){
        this.order = order;
        product = new Fishburger();
    }

    @Override
    public void execute() {
        order.addProduct(product);
        order.addCommandToHistory(this);
    }

    @Override
    public Product getProduct() {
        return product;
    }
}
