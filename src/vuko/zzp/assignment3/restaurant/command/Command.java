package vuko.zzp.assignment3.restaurant.command;

import vuko.zzp.assignment3.restaurant.Product;

public interface Command {
    void execute();
    Product getProduct();
}
