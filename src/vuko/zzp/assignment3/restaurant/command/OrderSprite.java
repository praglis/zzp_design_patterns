package vuko.zzp.assignment3.restaurant.command;

import vuko.zzp.assignment3.restaurant.Order;
import vuko.zzp.assignment3.restaurant.Product;
import vuko.zzp.assignment3.restaurant.drink.Sprite;

public class OrderSprite implements Command {
    Order order;
    Product product;

    public OrderSprite(Order order){
        this.order = order;
        product = new Sprite();
    }

    @Override
    public void execute() {
        order.addProduct(product);
        order.addCommandToHistory(this);
    }

    @Override
    public Product getProduct() {
        return product;
    }
}
