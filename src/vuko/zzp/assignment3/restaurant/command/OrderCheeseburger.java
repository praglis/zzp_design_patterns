package vuko.zzp.assignment3.restaurant.command;

import vuko.zzp.assignment3.restaurant.Order;
import vuko.zzp.assignment3.restaurant.Product;
import vuko.zzp.assignment3.restaurant.burger.Cheeseburger;

public class OrderCheeseburger implements Command{
    Order order;
    Product product;

    public OrderCheeseburger(Order order){
        this.order = order;
        product = new Cheeseburger();
    }

    @Override
    public void execute() {
        order.addProduct(product);
        order.addCommandToHistory(this);
    }

    @Override
    public Product getProduct() {
        return product;
    }
}
