package vuko.zzp.assignment3.restaurant.command;

import vuko.zzp.assignment3.restaurant.Order;
import vuko.zzp.assignment3.restaurant.Product;
import vuko.zzp.assignment3.restaurant.drink.Pepsi;

public class OrderPepsi implements Command {
    Order order;
    Product product;

    public OrderPepsi(Order order){
        this.order = order;
        product = new Pepsi();
    }

    @Override
    public void execute() {
        order.addProduct(product);
        order.addCommandToHistory(this);
    }

    @Override
    public Product getProduct() {
        return product;
    }
}
