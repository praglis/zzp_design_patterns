package vuko.zzp.assignment3.restaurant;

import vuko.zzp.assignment3.restaurant.command.Command;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public class Order {
    private List<Product> orderList;
    private List<Command> orderHistory;
    private BigDecimal orderCost;

    public Order() {
        orderList = new LinkedList<>();
        orderHistory = new LinkedList<>();
        this.orderCost = BigDecimal.ZERO;
    }

    public BigDecimal getOrderCost() {
        return orderCost;
    }

    public void setOrderCost(BigDecimal orderCost) {
        this.orderCost = orderCost;
    }

    public List<Product> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Product> orderList) {
        this.orderList = orderList;
    }

    public List<Command> getOrderHistory() {
        return orderHistory;
    }

    public void setOrderHistory(List<Command> orderHistory) {
        this.orderHistory = orderHistory;
    }

    public void addProduct(Product product) {
        orderList.add(product);

    }

    public void addCommandToHistory(Command command) {
        orderHistory.add(command);
        orderCost = orderCost.add(command.getProduct().getCost());
    }
}
