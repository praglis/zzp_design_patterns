package vuko.zzp.assignment2.houseapi;

import vuko.zzp.assignment2.houseapi.light.Light;
import vuko.zzp.assignment2.houseapi.light.LightColorDecorator;
import vuko.zzp.assignment2.houseapi.light.LightIntensivityDecorator;

public class House {
    private Light houseLight;
    private Light gardenLight;
    private Light homeCinemaLight;
    private Light gateLight;
    private Door door;
    private Door gate;
    private Heater heater;

    public House() {
        this.houseLight = new Light();
        this.gardenLight = new Light();
        this.homeCinemaLight = new Light();
        this.gateLight = new Light();
        this.door = new Door();
        this.gate = new Door();
        this.heater = new Heater();
    }

    public void setLightOn() {
        houseLight.setLightOn(true);
        gardenLight.setLightOn(true);
    }

    public void turnOffAllLights() {
        houseLight.setLightOn(false);
        homeCinemaLight.setLightOn(false);
        gardenLight.setLightOn(false);
        gateLight.setLightOn(false);
    }

    public void openDoor() {
        door.setDoorLocked(false);
        door.setDoorClosed(false);
    }

    public void lockAllDoors() {
        lockDoor();
        gate.setDoorClosed(true);
        gate.setDoorLocked(true);
    }

    public void lockDoor() {
        door.setDoorClosed(true);
        door.setDoorLocked(true);
    }

    private void changeTemperature(int changeValue) {
        if (!heater.isHeaterOn()) setTemperature(20);
        heater.setTemperature(heater.getTemperature() + changeValue);
    }

    public void setTemperature(int temperature) {
        heater.setHeaterOn(true);
        heater.setTemperature(temperature);
    }

    public void enterHouseInWinter(boolean isDay) {
        openDoor();
        if (isDay) setTemperature(22);
        else {
            setTemperature(24);
            setLightOn();
        }
    }

    public void exitHouseInWinter() {
        setTemperature(15);
        turnOffAllLights();
        lockDoor();
    }

    public void enterHouseInSummer(boolean isDay) {
        openDoor();
        if (isDay) setTemperature(16);
        else {
            setTemperature(18);
            setLightOn();
        }
    }

    public void exitHouseInSummer() {
        heater.setHeaterOn(false);
        turnOffAllLights();
        lockDoor();
    }

    public void goToSleep(boolean isWinter) {
        lockAllDoors();
        if (isWinter) changeTemperature(-3);
        else changeTemperature(-1);
        turnOffAllLights();
    }


    public void wakeUp(boolean isWinter) {
        if (isWinter) {
            changeTemperature(+3);
            setLightOn();
        } else changeTemperature(+1);
    }

    public void watchMovie() {
        lockAllDoors();
        turnOffAllLights();

        homeCinemaLight.setLightOn(true);
        LightColorDecorator redCinema = new LightColorDecorator(homeCinemaLight);
        redCinema.setLightColor("red");

        LightIntensivityDecorator moodyCinema = new LightIntensivityDecorator(redCinema);
        moodyCinema.setIntensivity(30);
    }

    public void goWatchStars() {
        openDoor();

        houseLight.setLightOn(false);
        LightIntensivityDecorator dimmedGarden = new LightIntensivityDecorator(gardenLight);
        dimmedGarden.setIntensivity(50);
    }

    public void parkCar(boolean isDay) {
        if (!isDay) gateLight.setLightOn(true);
        gate.setDoorLocked(false);
        gate.setDoorClosed(false);
    }

    public void driveToWork() {
        gate.setDoorClosed(true);
        gate.setDoorLocked(true);
    }
}
