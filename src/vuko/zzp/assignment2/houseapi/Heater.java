package vuko.zzp.assignment2.houseapi;

public class Heater {
    private boolean isHeaterOn;
    private int temperature;

    public Heater(){
        isHeaterOn = false;
        temperature = 20;
    }

    public boolean isHeaterOn() {
        return isHeaterOn;
    }

    public void setHeaterOn(boolean heaterOn) {
        isHeaterOn = heaterOn;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }
}
