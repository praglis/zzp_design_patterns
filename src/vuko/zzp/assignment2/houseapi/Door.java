package vuko.zzp.assignment2.houseapi;

public class Door {
    private boolean isDoorClosed;
    private boolean isDoorLocked;

    public Door() {
        this.isDoorClosed = true;
        this.isDoorLocked = true;
    }

    public void setDoorClosed(boolean doorClosed) {
        isDoorClosed = doorClosed;
    }

    public void setDoorLocked(boolean doorLocked) {
        isDoorLocked = doorLocked;
    }
}
