package vuko.zzp.assignment2.houseapi.light;

public class LightColorDecorator extends Light {
    private Light light;
    private String lightColor;

    public LightColorDecorator(Light light){
        this.light = light;
    }

    public String getLightColor() {
        return lightColor;
    }

    public void setLightColor(String lightColor) {
        this.lightColor = lightColor;
    }
}

