package vuko.zzp.assignment2.houseapi.light;

public class Light {
    private boolean isLightOn = false;

    public boolean isLightOn() {
        return isLightOn;
    }

    public void setLightOn(boolean lightOn) {
        isLightOn = lightOn;
    }
}
