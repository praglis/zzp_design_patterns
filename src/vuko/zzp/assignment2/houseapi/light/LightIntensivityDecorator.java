package vuko.zzp.assignment2.houseapi.light;

public class LightIntensivityDecorator extends Light {
    private Light light;
    private int intensivity;

    public LightIntensivityDecorator(Light light) {
        this.light = light;
    }

    public int getIntensivity() {
        return intensivity;
    }

    public void setIntensivity(int intensivity) {
        this.intensivity = intensivity;
    }
}

