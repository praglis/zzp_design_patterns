package vuko.zzp;

import vuko.zzp.assignment1.builder.CalzonePizzaBuilder;
import vuko.zzp.assignment1.builder.NewyorkerPizzaBuilder;
import vuko.zzp.assignment1.pizza.Pizza;
import vuko.zzp.assignment2.houseapi.House;
import vuko.zzp.assignment3.restaurant.OrderMachine;
import vuko.zzp.assignment3.restaurant.observer.KitchenDepartment;

public class Main {

    public static void main(String[] args) {
        final boolean SAUCE_OUTSIDE = true;
        final boolean SAUCE_INSIDE = false;

        NewyorkerPizzaBuilder newyorkerPizzaBuilder = new NewyorkerPizzaBuilder("duży", "szynka", "pomidorowy");
        newyorkerPizzaBuilder.addOptionalIngredient("ser");
        Pizza newyorkerPizza = newyorkerPizzaBuilder.buildPizza();

        CalzonePizzaBuilder calzonePizzaBuilder = new CalzonePizzaBuilder("średni", "kiełbasa", "czosnkowy", SAUCE_OUTSIDE);
        calzonePizzaBuilder.addOptionalIngredient("cebula").addOptionalIngredient("pieczarki");
        Pizza calzonePizza = calzonePizzaBuilder.buildPizza();

        House house = new House();
        house.wakeUp(true);
        house.exitHouseInWinter();
        house.driveToWork();
        house.parkCar(false);
        house.enterHouseInWinter(false);
        house.goWatchStars();
        house.watchMovie();
        house.goToSleep(true);

        OrderMachine orderMachine = new OrderMachine();
        orderMachine.registerObserver(new KitchenDepartment());

        orderMachine.newOrder();
        orderMachine.addPepsi();
        orderMachine.addFishburger();
        orderMachine.finalizeOrder();

        orderMachine.newOrder();
        orderMachine.addPepsi();
        orderMachine.addHamburger();
        orderMachine.addHamburger();
        orderMachine.finalizeOrder();

        orderMachine.newOrder();
        orderMachine.addCheeseburger();
        orderMachine.addFishburger();
        orderMachine.addCheeseburger();
        orderMachine.addPepsi();
        orderMachine.finalizeOrder();

        orderMachine.newOrder();
        orderMachine.addFanta();
        orderMachine.addFishburger();
        orderMachine.addCheeseburger();
        orderMachine.addPepsi();
        orderMachine.finalizeOrder();

        orderMachine.newOrder();
        orderMachine.addHamburger();
        orderMachine.addHamburger();
        orderMachine.finalizeOrder();
    }


}
